/**
 * Created by Tomas Michalik on 27.11.16.
 */
public interface Command {
    public void execute();
}
