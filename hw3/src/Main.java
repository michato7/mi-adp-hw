/**
 * Created by Tomas Michalik on 27.11.16.
 */
public class Main {

    public static void main(String[] args){

        // init command timer
        CommandsTimer timer = new CommandsTimer();

        // example commands
        ConsoleCommand consoleCommand = new ConsoleCommand();
        ConsoleCommand consoleCommand2 = new ConsoleCommand();
        ConsoleCommand consoleCommand3 = new ConsoleCommand();
        FileOutCommand fileOutCommand = new FileOutCommand("test");
        FileOutCommand fileOutCommand2 = new FileOutCommand("test");

        // add commands to timer
        timer.addCommand(consoleCommand, 1);
        timer.addCommand(consoleCommand2, 4);
        timer.addCommand(consoleCommand3, 8);
        timer.addCommand(fileOutCommand, 2);
        timer.addCommand(fileOutCommand2, 6);

        // start all commands
        timer.start();

        // commands execution testing - check std out
        try {
            Thread.sleep(10*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // remove one command
        timer.removeCommand(fileOutCommand);

        // commands execution testing - check std out
        try {
            Thread.sleep(10*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // stop timer
        timer.stop();
        timer.join();
    }
}
