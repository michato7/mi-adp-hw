import java.util.*;

/**
 * Created by Tomas Michalik on 27.11.16.
 */
public class CommandsTimer {
    Map<Command, Thread> queue;

    public CommandsTimer() {
        this.queue = new HashMap<Command, Thread>();
    }

    public void start(){
        for(Map.Entry<Command, Thread> entry : queue.entrySet()){
            entry.getValue().start();
        }
    }

    public void stop(){
        for(Map.Entry<Command, Thread> entry : queue.entrySet()){
            entry.getValue().interrupt();
        }
    }

    public void addCommand(Command cmd, int period){
        Thread thr = new Thread(){
            public void run(){
                while(true){
                    cmd.execute();
                    try {
                        sleep(period * 1000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        };

        queue.put(cmd, thr);
    }

    public Boolean removeCommand(Command cmd){
        Thread thr = queue.remove(cmd);
        if(thr == null){
            return false;
        }
        thr.interrupt();
        return true;
    }

    public void join(){
        for(Map.Entry<Command, Thread> entry : queue.entrySet()){
            try {
                entry.getValue().join();
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
