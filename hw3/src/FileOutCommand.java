import java.io.*;

/**
 * Created by Tomas Michalik on 27.11.16.
 */
public class FileOutCommand implements Command {
    private String fileName;

    public FileOutCommand(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void execute() {
        String str = "timer test string " + Thread.currentThread().getId();

        try(FileWriter fw = new FileWriter(fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            System.out.println("FileOutCommand file write, thr: " + Thread.currentThread().getId());
            out.println(str);
        } catch (IOException e) {
            System.out.println("file write error");
        }
    }
}
