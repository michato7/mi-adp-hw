/**
 * Created by Tomas Michalik on 27.11.16.
 */
public class ConsoleCommand implements Command {

    @Override
    public void execute() {
        System.out.println("ConsoleCommand console output, thr: " + Thread.currentThread().getId());
    }
}
