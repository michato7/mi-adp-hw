public interface Strategy {
    public boolean put(Key key, Data data);
    public Data get(Key key);
    public boolean remove(Key key);
    public int size();
}
