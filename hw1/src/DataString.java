public class DataString implements Data {

    private String data;

    public DataString(String data){
        this.data = data;
    }

    public String toString(){
        return data;
    }

}
