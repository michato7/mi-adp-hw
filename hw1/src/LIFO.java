public class LIFO implements Strategy {

    private int size;
    private int currentSize;
    private Key[] keys;
    private Data[] datas;

    public LIFO(int size){
        this.size = size;
        keys = new Key[size];
        datas = new Data[size];
        currentSize = 0;
    }

    @Override
    public boolean put(Key key, Data data) {

        if(currentSize + 1 > size){
            return false;
        }

        keys[currentSize] = key;
        datas[currentSize] = data;
        currentSize++;

        return true;
    }

    @Override
    public Data get(Key key) {

        // simulation of indirect access
        for(int i=currentSize-1;i>=0;i--){
            if( keys[i] == key ){
                return datas[i];
            }
        }

        return null;
    }

    @Override
    public boolean remove(Key key) {

        // simulation of indirect access
        for(int i=currentSize-1;i>=0;i--){
            if( keys[i] == key ){
                // shift elems to empty space
                for(int a=i;a<currentSize-1;a++){
                    keys[a] = keys[a+1];
                    datas[a] = datas[a+1];
                }
                currentSize--;
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return currentSize;
    }
}
