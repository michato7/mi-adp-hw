public class KeyInt implements Key {

    private int key;

    public KeyInt(int key){
        this.key = key;
    }

    public String toString(){
        return Integer.toString(key);
    }

}
