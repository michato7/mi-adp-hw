public class Cache {

    private Strategy strategy;

    public Cache(Strategy strategy){
        this.strategy = strategy;
    }

    public boolean put(Key key, Data data){
        return strategy.put(key, data);
    }

    public Data get(Key key){
        return strategy.get(key);
    }

    public int size(){
        return strategy.size();
    }

    public boolean remove(Key key){
        return strategy.remove(key);
    }
}
