public class Main {

    public static void main(String [] args){

        // test LIFO strategy
        testLIFO();

        // test FIFO strategy
        testFIFO();

    }

    private static void testLIFO(){

        Strategy strategy = new LIFO(5);

        Key key1 = new KeyInt(1);
        Data data1 = new DataString("Test data string 1");

        Key key2 = new KeyInt(2);
        Data data2 = new DataString("Test data string 2");

        Key key3 = new KeyInt(3);
        Data data3 = new DataString("Test data 2");

        Cache cache = new Cache(strategy);
        cache.put(key1, data1);
        cache.put(key2, data2);
        cache.put(key3, data3);

        System.out.println("==================== TEST od LIFO strategy ====================");

        // test of get method
        System.out.println("> Test of get() method");
        System.out.println( "Data: " + cache.get(key1) + " key: " + key1 );
        System.out.println( "Data: " + cache.get(key3) + " key: " + key3 );

        // test of missing key
        Key key42 = new KeyInt(42);
        System.out.println("\n> Test of missing key");
        System.out.println( "Data: " + cache.get(key42));

        // test of remove missing key
        System.out.println("\n> Test of remove missing key");
        System.out.println( "Removed? " + cache.remove(key42));

        // test of cache capacity
        System.out.println("\n> Test of cache capacity");
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(4), new DataString("test str")));
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(5), new DataString("test str")));
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(6), new DataString("test str")));

    }

    private static void testFIFO(){

        Strategy strategy = new FIFO(5);

        Key key1 = new KeyInt(1);
        Data data1 = new DataString("Test data string 1");

        Key key2 = new KeyInt(2);
        Data data2 = new DataString("Test data string 2");

        Key key3 = new KeyInt(3);
        Data data3 = new DataString("Test data 2");

        Cache cache = new Cache(strategy);
        cache.put(key1, data1);
        cache.put(key2, data2);
        cache.put(key3, data3);

        System.out.println("\n\n==================== TEST od FIFO strategy ====================");

        // test of get method
        System.out.println("> Test of get() method");
        System.out.println( "Data: " + cache.get(key1) + " key: " + key1 );
        System.out.println( "Data: " + cache.get(key3) + " key: " + key3 );

        // test of missing key
        Key key42 = new KeyInt(42);
        System.out.println("\n> Test of missing key");
        System.out.println( "Data: " + cache.get(key42));

        // test of remove missing key
        System.out.println("\n> Test of remove missing key");
        System.out.println( "Removed? " + cache.remove(key42));

        // test of cache capacity
        System.out.println("\n> Test of cache capacity");
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(4), new DataString("test str")));
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(5), new DataString("test str")));
        System.out.println( "Added new key:data ? " + cache.put(new KeyInt(6), new DataString("test str")));

    }

}
