package main;

import client.Client;
import obj.LObject;
import proxy.LObjectContainmentProxy;

public class Ukol2 {
    public static void main(String[] args) throws InterruptedException {
        LObjectContainmentProxy prx = new LObjectContainmentProxy();

        prx.addLObject((new LObject("alfa")), 1);
        prx.addLObject((new LObject("beta")), 2);
        prx.addLObject((new LObject("gamma")), 3);
        prx.addLObject((new LObject("delta")), 4);
        prx.addLObject((new LObject("echo")), 5);

        Client c1, c2, c3, c4;
        c1 = new Client( prx , 1 );
        c2 = new Client( prx , 2 );
        c3 = new Client( prx , 3 );
        c4 = new Client( prx , 4 );

        c1.start();
        c2.start();
        c3.start();
        c4.start();

        c1.join();
        c2.join();
        c3.join();
        c4.join();
    }
}
