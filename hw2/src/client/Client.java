package client;

import obj.LObject;
import proxy.LObjectContainmentProxy;

public class Client extends Thread {
    LObjectContainmentProxy prx;
    int m_ID;

    public Client ( LObjectContainmentProxy prx , int id ) {
        this.prx = prx;
        m_ID = id;
    }

    @Override
    public void run() {
        LObject obj, obj2, toAdd;

        System.out.println("Clinet #" + m_ID + " is reading");
        obj = prx.read(2);
        System.out.println("Client: #" + m_ID + " | Object: " + obj.getName() + " was read");
        obj = prx.read(3);
        System.out.println("Client: #" + m_ID + " | Object: " + obj.getName() + " was read");
        toAdd = new LObject( m_ID + "-ADDED");
        prx.addLObject( toAdd , m_ID+5 );
        obj2 = prx.read( m_ID+5 );
        System.out.println("Client: #" + m_ID + " | Object: " + obj2.getName() + " was recieved");
    }
}
