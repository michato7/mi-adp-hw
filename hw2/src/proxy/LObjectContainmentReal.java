package proxy;


import obj.LObject;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LObjectContainmentReal extends Hashtable implements LObjectContainment {

    int contRef = 0;

    @Override
    public LObject read(Object key) {
        try {
            Thread.sleep(2500);
        } catch ( InterruptedException e ) {
            Logger.getLogger( LObjectContainmentReal.class.getName() ).log( Level.SEVERE , null , e );
        }
        return (LObject)this.get( key );
    }

    @Override
    public boolean removeLObject(Object key, Object val) {
        return this.remove( key , val );
    }

    @Override
    public Object addLObject(LObject toAdd, Object key) {
        return this.put( key , toAdd );
    }

    public synchronized void addProxy ( ) {
        contRef++;
    }

    public synchronized void remProxy ( ) {
        contRef--;
    }

    public int getProxyCnt() {
        return contRef;
    }

    public synchronized Object clone( ) {
        LObjectContainmentReal cpy;
        cpy = ( LObjectContainmentReal ) super.clone( );
        cpy.contRef = 1;
        return cpy;
    }
}
