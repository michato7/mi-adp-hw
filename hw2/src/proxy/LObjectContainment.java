package proxy;

import obj.LObject;

interface LObjectContainment {
    LObject read ( Object key );
    boolean removeLObject ( Object key, Object val );
    Object addLObject ( LObject toAdd , Object key );
}
