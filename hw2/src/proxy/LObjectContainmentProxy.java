package proxy;


import obj.LObject;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LObjectContainmentProxy implements LObjectContainment {

    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock rLock = rwLock.readLock();
    private final Lock wLock = rwLock.writeLock();

    private LObjectContainmentReal cont;

    public LObjectContainmentProxy( ) {
        cont = new LObjectContainmentReal();
    }

    public LObjectContainmentProxy(LObjectContainmentReal cont) {
        this.cont = cont;
    }

    @Override
    public LObject read(Object key) {
        LObject toRead;
        rLock.lock();
        try {
            toRead = cont.read( key );
        } finally {
            rLock.unlock();
        }
        return toRead;
    }

    @Override
    public boolean removeLObject(Object key, Object val) {
        wLock.lock();
        try {
            cpyOnWr();
        } finally {
            wLock.unlock();
        }
        return cont.removeLObject( key , val );
    }

    @Override
    public Object addLObject(LObject toAdd, Object key) {
        wLock.lock();
        try {
            cpyOnWr();
        } finally {
            wLock.unlock();
        }
        return cont.addLObject( toAdd , key );
    }

    private void cpyOnWr ( ) {
        if ( cont.getProxyCnt() > 1 ) {
            synchronized ( cont ) {
                cont.remProxy( );
                try {
                    cont = ( LObjectContainmentReal ) cont.clone();
                } catch ( Throwable e ){
                    System.out.println(e.getStackTrace());
                }
                cont.addProxy();
            }
        }
    }

    public synchronized Object clone ( ) {
        Object cpy = new LObjectContainmentProxy( cont );
        cont.addProxy();
        return cpy;
    }

}
